ffvii(personnage(aeris,cetras,soin,19,10000),ifalna).
ffvii(personnage(ifalna,cetras,pnj,6,15000),cloud).
ffvii(personnage(cloud,soldat,heros,18,30000),cloud).
ffvii(personnage(tifa,equipe,guerrier,11,15000),barret).
ffvii(personnage(barret,equipe,guerrier,7,14000),cloud).
ffvii(personnage(rougexiii,equipe,soin,18,11000),barret).
ffvii(personnage(yuffie,equipe,ninja,8,2000),barret).

/*
	Question a -> trouver le groupe dans lequel une personne Èvolue.
	P -> Personnage
	G -> Groupe que l'on cherche
*/
groupe(P,G):-ffvii(personnage(P,G,_,_,_),_).

/*
	Question b -> Ètant donnÈ le nom d'une personne, trouver qui est le chef du groupe dans lequel elle Èvolue.
	P -> Personnage
	C -> Chef du groupe que l'on cherche
*/
chef(P,C):-ffvii(personnage(P,_,_,_,_),C).

/*
	Question c -> la structure de líÈquipe Ètant hiÈrarchique, on doit pouvoir remonter depuis n'importe quel membre vers le chef (Cloud). Le prÈdicat membre_valide permettra de vÈrifier qu'un membre est bien sous les ordres de Cloud en remontant la chaine hiÈrarchique.
	P -> Personnage
	Fonction rÈcursive qui va chercher le chef tant que ce n'est pas cloud. Si l'on arrive pas ‡ Cloud, renvoie false, sinon renvoie true.
*/
membre_valide(cloud).
membre_valide(P):-chef(P,C),membre_valide(C).

/*
	Question d -> donne le niveau díexperience d'un membre
	P -> Personnage
	N -> Niveau d'expÈrience que l'on cherche
*/
xp(P,N):-ffvii(personnage(P,_,_,_,N),_).

/*
	Question ei -> donne le niveau díexpÈrience d'un membre en ajoutant au niveau de base un bonus, en utilisant les rËgles suivantes :
					i) tous les membres ayant une cÙte de popularitÈ de 10 ou plus ont un bonus de 5000.
	P -> Personnage
	N -> Note du personnage que l'on cherche
*/
note(P,N):-ffvii(personnage(P,_,_,N,_),_).

/*
	P -> Personnage
	NR -> Note RÈelle du personnage que l'on cherche
	Si la note est infÈrieure ‡ 10, l'XP rÈelle est l'XP du personnage
*/
add_xp(P,NR):-note(P,NOTE),NOTE < 10,xp(P,NR).

/*
	P -> Personnage
	NR -> Note RÈelle du personnage que l'on cherche
	Si la note est infÈrieure ‡ 10, l'XP rÈelle est l'XP du personnage + 5000
*/
add_xp(P,NR):-note(P,NOTE),NOTE >= 10,xp(P,N), NR is N+5000.

/*
	Question eii -> donne le niveau díexpÈrience d'un membre en ajoutant au niveau de base un bonus, en utilisant les rËgles suivantes :
					ii) Aucun personnage ne peut avoir plus díexpÈrience que son chef de groupe (attention le cas de Cloud est Èvidemment spÈcial).
	P -> Personnage
	NR -> Note RÈelle du personnage que l'on cherche
*/
/*
	P -> Personnage
	NR -> Note RÈelle du personnage que l'on cherche
	Si l'expÈrience du personnage est supÈrieure ‡ l'expÈrience du chef, l'expÈrence rÈelle du personnage prendra la valeur de l'expÈrience rÈelle du chef
*/
xp_reelle(P,NR):-chef(P,C),add_xp(C,XPC),add_xp(P,XP), XP > XPC, NR is XPC.

/*
	P -> Personnage
	NR -> Note RÈelle du personnage que l'on cherche
	Si l'expÈrience du personnage est infÈrieure ou Ègale ‡ l'expÈrience du chef, l'expÈrence rÈelle du personnage ne changera pas
*/
xp_reelle(P,NR):-chef(P,C),add_xp(C,XPC),add_xp(P,XP), XP =< XPC, NR is XP.


%QUESTION 2 : factorielle
/*
	N -> Nombre dont on veux calculer la factorielle
	R -> RÈsultat de la factorielle 
	Tant que N est supÈrieur ‡ 1, on rappelle la fonction factorielle en dÈcrÈmentant N de 1 et en multipliant le rÈsultat prÈcÈdemment trouvÈ par le rÈsultat de cette fonction.
*/
factorielle(N,R):- X is N-1,N > 1,factorielle(X,R2), R is N * R2.

/*
	Lorsque N atteint 1, on renvoie le rÈsultat (on multiplie par 1 histoire de dire)
*/
factorielle(N,R):- N =< 1, R is N * 1.


%QUESTION 3 : Suite de Lucas

/*
	On vÈrifie que P et Q sont valides avec la rËgle P≤-4Q =! 0
*/
is_valid(P,Q):- Carre is P*P, Produit is 4*Q, Somme is Carre - Produit, Somme \=0.

u(_,_,0,0).
u(_,_,1,1).
u(P,Q,N,R):-N > 1, NM1 is N - 1,NM2 is N - 2, u(P,Q,NM1,RNM1), u(P,Q,NM2,RNM2), Produit1 is P*RNM1, Produit2 is Q*RNM2, R is Produit1 - Produit2.

v(_,_,0,2).
v(P,_,1,P).
v(P,Q,N,R):-N > 1, NM1 is N - 1,NM2 is N - 2, v(P,Q,NM1,RNM1), v(P,Q,NM2,RNM2), Produit1 is P*RNM1, Produit2 is Q*RNM2, R is Produit1 - Produit2.

lucas(P,Q,_):-not(is_valid(P,Q)), write('P et Q ne sont pas valides'),fail.
lucas(P,Q,N):-is_valid(P,Q),u(P,Q,N,RU),v(P,Q,N,RV),write('U('),write(N),write(')='),write(RU),write('\n'),write('V('),write(N),write(')='),write(RV).